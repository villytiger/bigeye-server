/* BigEye Server
 * Copyright (C) 2020 Ilya Lyubimov <villytiger>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "settings.h"

static gboolean load_options(int argc, char* argv[], GOptionEntry* entries, const char** config_path,
                              GError** error) {
        GOptionEntry config_entries[] = {
                {"config", 'c', 0, G_OPTION_ARG_FILENAME, config_path, "Path to configuration file", "CONFIG_PATH"},
                {NULL},
        };

        GOptionGroup* group = g_option_group_new("config", "Configuration", "Show configuration options", NULL, NULL);
        g_option_group_add_entries(group, config_entries);

        g_autoptr(GOptionContext) optctx = g_option_context_new("<launch line> - BigEye RTSP Server");
        g_option_context_add_main_entries(optctx, entries, NULL);
        g_option_context_add_group(optctx, group);

        GError* e = NULL;
        gboolean b = g_option_context_parse(optctx, &argc, &argv, &e);
        if (!b) {
                g_propagate_prefixed_error(error, e, "Error parsing options: ");
                return FALSE;
        }

        return TRUE;
}

static gboolean load_callback_from_config(GKeyFile* key_file, GOptionEntry* entry, GError** error) {
        g_autofree gchar* s = g_key_file_get_string(key_file, "Application", entry->long_name, error);
        if (!s) return FALSE;
        return ((GOptionArgFunc)entry->arg_data)(entry->long_name, s, NULL, error);
}

static gboolean load_filename_from_config(GKeyFile* key_file, GOptionEntry* entry, GError** error) {
        g_autofree gchar* s = g_key_file_get_string(key_file, "Application", entry->long_name, error);
        gchar* fn = g_filename_from_utf8(s, -1, NULL, NULL, error);
        *(gchar**)entry->arg_data = fn;
        return fn != NULL;
}

static gboolean load_filename_array_from_config(GKeyFile* key_file, GOptionEntry* entry, GError** error) {
        gsize count;
        g_auto(GStrv) sl = g_key_file_get_string_list(key_file, "Application", entry->long_name, &count, error);
        if (!sl) return FALSE;

        gchar** fl = g_malloc0_n(count, sizeof(gchar*));
        for (gsize i = 0; i != count; ++count) {
                fl[i] = g_filename_from_utf8(sl[i], -1, NULL, NULL, error);
                if (!fl[i]) {
                        g_strfreev(fl);
                        return FALSE;
                }
        }

        *(gchar***)entry->arg_data = fl;

        return TRUE;
}

static gboolean load_option_from_config(GKeyFile* key_file, GOptionEntry* entry, GError** error) {
        switch (entry->arg) {
        case G_OPTION_ARG_NONE:
                *(gboolean*)entry->arg_data =
                        g_key_file_get_boolean(key_file, "Application", entry->long_name, error);
                break;
        case G_OPTION_ARG_STRING:
                *(const gchar**)entry->arg_data =
                        g_key_file_get_string(key_file, "Application", entry->long_name, error);
                break;
        case G_OPTION_ARG_INT:
                *(gint*)entry->arg_data =
                        g_key_file_get_integer(key_file, "Application", entry->long_name, error);
                break;
        case G_OPTION_ARG_CALLBACK:
                return load_callback_from_config(key_file, entry, error);
        case G_OPTION_ARG_FILENAME:
                return load_filename_from_config(key_file, entry, error);
        case G_OPTION_ARG_STRING_ARRAY:
                *(gchar***)entry->arg_data =
                        g_key_file_get_string_list(key_file, "Application", entry->long_name, NULL, error);
                break;
        case G_OPTION_ARG_FILENAME_ARRAY:
                return load_filename_array_from_config(key_file, entry, error);
        case G_OPTION_ARG_DOUBLE:
                *(gdouble*)entry->arg_data =
                        g_key_file_get_double(key_file, "Application", entry->long_name, error);
                break;
        case G_OPTION_ARG_INT64:
                *(gdouble*)entry->arg_data =
                        g_key_file_get_int64(key_file, "Application", entry->long_name, error);
                break;
        }

        return *error == NULL;
}

static gboolean load_config(const gchar* config_path, GOptionEntry* entries, GError** error) {
        gboolean b;
        GError* e = NULL;

        g_autoptr(GKeyFile) kf = g_key_file_new();

        b = g_key_file_load_from_file(kf, config_path, G_KEY_FILE_NONE, &e);
        if (!b) {
                g_propagate_prefixed_error(error, e, "Error loading config file: ");
                return FALSE;
        }

        for (; entries->long_name; ++entries) {
                e = NULL;
                b = g_key_file_has_key(kf, "Application", entries->long_name, &e);
                if (!b) {
                        if (!e) continue;
                        g_propagate_prefixed_error(error, e, "Error loading config file: ");
                        return FALSE;
                }

                b = load_option_from_config(kf, entries, &e);
                if (!b) {
                        g_propagate_prefixed_error(error, e, "Error loading config file: ");
                        return FALSE;
                }
        }

        return TRUE;
}

gboolean load_settings(int argc, char* argv[], GOptionEntry* entries, GError** error) {
        gboolean b;

        g_autofree const gchar* config_path = NULL;
        b = load_options(argc, argv, entries, &config_path, error);
        if (!b) return FALSE;

        if (config_path) return load_config(config_path, entries, error);

        return TRUE;
}

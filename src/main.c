/* BigEye Server
 * Copyright (C) 2020 Ilya Lyubimov <villytiger>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <gst/gst.h>

#include <gst/rtsp-server/rtsp-server.h>

#include "settings.h"

struct {
        const gchar* service;
        const gchar* pipeline;
} settings;

static void init_settings(int argc, char* argv[]) {
        gint port = 8554;
        const gchar* pipeline = NULL;
        const gchar* video = NULL;
        const gchar* audio = NULL;
        GOptionEntry entries[] = {
                {"port", 'p', 0, G_OPTION_ARG_INT, &port, "Port to listen on (default: 8554)", "PORT"},
                {"video", 'v', 0, G_OPTION_ARG_STRING, &video, "V4L2 device (default: /dev/video0)", "V4L2_DEVICE"},
                {"audio", 'a', 0, G_OPTION_ARG_STRING, &audio, "ALSA device (default: default:1)", "ALSA_DEVICE"},
                {"pipeline", 0, 0, G_OPTION_ARG_STRING, &pipeline, "Gstreamer pipeline", "PIPELINE"},
                {NULL},
        };

        GError* error = NULL;
        gboolean b = load_settings(argc, argv, entries, &error);
        if (!b) {
                g_printerr("%s\n", error->message);
                exit(1);
        }

        if (port < 1 || port > 65535) {
                g_printerr("Error loading settings: Wrong port number: %d\n", port);
                exit(1);
        }

        settings.service = g_strdup_printf("%i", port);

        if (pipeline) {
                if (video) {
                        g_printerr("Error loading settings: -video option is not compatible with -pipeline");
                        exit(1);
                }

                if (audio) {
                        g_printerr("Error loading settings: -audio option is not compatible with -pipeline");
                        exit(1);
                }

                settings.pipeline = pipeline;
        } else {
                if (!video) video = "/dev/video0";
                if (!audio) audio = "default:1";

                pipeline =
                        "v4l2src device=%s ! h264parse ! rtph264pay name=pay0 pt=96 ! "
                        "alsasrc device=%s ! avenc_aac ! rtpmp4apay name=pay1 pt=97";

                settings.pipeline = g_strdup_printf(pipeline, video, audio);
        }
}

int main(int argc, char* argv[]) {
        init_settings(argc, argv);

        gst_init(NULL, NULL);

        GstRTSPServer* server = gst_rtsp_server_new();
        g_object_set(server, "service", settings.service, NULL);

        GstRTSPMediaFactory* factory = gst_rtsp_media_factory_new();
        gst_rtsp_media_factory_set_launch(factory, settings.pipeline);
        gst_rtsp_media_factory_set_shared(factory, TRUE);

        GstRTSPMountPoints* mounts = gst_rtsp_server_get_mount_points(server);
        gst_rtsp_mount_points_add_factory(mounts, "/unicast", factory);
        g_object_unref(mounts);

        g_autoptr(GMainLoop) loop = g_main_loop_new(NULL, FALSE);
        gst_rtsp_server_attach(server, NULL);
        g_main_loop_run(loop);

        return 0;
}
